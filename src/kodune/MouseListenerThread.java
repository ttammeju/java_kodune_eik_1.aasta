package kodune;

import java.awt.*;

/**
 * Monitors mouse. Checking if its coordinates are inside the main (screen) window
 *
 * @author Tarvo Tammejuur
 * @version 1.0
 */
public class MouseListenerThread extends Thread {
    private Game game = null;

    /**
     * Storing the reference of the current game
     *
     * @param game current game
     * @see Game
     */
    public MouseListenerThread(Game game) {
        this.game = game;
    }

    /**
     * This method ends the game if mouse is outside the main window bounds when game is running
     *
     * @see Game#isStarted()
     */
    @Override
    public void run() {
        while (true) {
            if (!isMouseOnFrame() && game.isStarted()) { // mouse otside the frame while gaming - it's a no-no!!!!
                System.out.println("Mouse out of bounds -- G A M E  L O C K E D !");
                game.endGame();
            }
        }
    }

    /**
     * Checks whether mouse is on window
     *
     * @return <i>true</i> if mouse on window
     * @see Mouse
     */
    public boolean isMouseOnFrame() {
        Point mPoint = Mouse.getPosition();
        boolean mouseOnFrameX = (mPoint.getX() > game.getScreen().getX()) && (mPoint.getX() < game.getScreen().getX() + game.getScreen().getWidth());
        boolean mouseOnFrameY = (mPoint.getY() > game.getScreen().getY()) && (mPoint.getY() < game.getScreen().getY() + game.getScreen().getHeight());

        return mouseOnFrameX && mouseOnFrameY;
    }
}