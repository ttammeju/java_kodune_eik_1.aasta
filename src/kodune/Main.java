package kodune;

/**
 * Main class, which starts the game
 *
 * @author Tarvo Tammejuur
 * @version 1.0
 */
public class Main {

    public static void main(String[] args) throws Exception {
        Screen screen = new Screen();
        Game game = new Game(screen);

        game.start();
    }
}