package kodune;

import javax.swing.*;
import java.awt.*;

/**
 * Class for creating and holding the map image
 *
 * @author Tarvo Tammejuur
 * @version 1.0
 */
public class MapImage {
    private static final int FRAME_DIMENSION = 300;
    private Image mapImg = null;

    /**
     * Creating the image from its parameter
     *
     * @param imgPath path to the image file
     */
    public MapImage(String imgPath) {
        try {
            this.mapImg = new ImageIcon(imgPath).getImage();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the Image object
     *
     * @return Image the constructor created otherwise <i>null</i>
     */
    public Image getImage() {
        if (this.isValidMap()) {
            return this.mapImg;
        }
        return null;
    }

    /**
     * Checking the validity(dimensions, right now, in this case) of the image
     *
     * @return <i>true</i> if image matches the frame/ jpanel dimensions
     */
    private boolean isValidMap() {
        return mapImg.getWidth(null) == FRAME_DIMENSION && mapImg.getHeight(null) == FRAME_DIMENSION;
    }
}
