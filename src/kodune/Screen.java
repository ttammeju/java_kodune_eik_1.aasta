package kodune;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * Main window where map image is load onto
 *
 * @author Tarvo Tammejuur
 * @version 1.0
 */
public class Screen extends JFrame {
    private ImagePanel panel = null;

    /**
     * Making the actual window.<br>
     * Initialising a window listener ({@link JFrame#addWindowListener(WindowListener)}) to be triggered when closing the screen. Before closing it will save user' best score to a file ({@link Score#writeToFile()})
     */
    public Screen() {
        super.setTitle("follow the path");
        super.setLocation(100, 200); // default is 0,0 (top left corner)
        super.setResizable(false);
        super.setVisible(true);

        super.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent windowEvent) {
                System.out.println("Saving to file");
                Score.writeToFile();
            }
        });
    }

    /**
     * getting the image panel
     *
     * @return image panel (map image attached to it)
     */
    public ImagePanel getPanel() {
        return panel;
    }

    /**
     * Constructs an image panel (see: {@link ImagePanel}) and loads it onto our screen window
     *
     * @param imgPath path where the image is located
     */
    public void loadMap(String imgPath) {
        this.panel = new ImagePanel(imgPath);
        super.getContentPane().add(panel);
        super.pack();
    }
}
