package kodune;

import javax.swing.*;
import java.awt.*;

/**
 * Jpanel({@link javax.swing.JPanel}) where map image is on. Constructing an image and drawing it on the screen (panel)
 *
 * @author Tarvo Tammejuur
 * @version 1.0
 */
class ImagePanel extends JPanel {
    private Image img;

    /**
     * Setting JPanel size to match image dimensions and initialises the img ({@link #img}) variable.
     *
     * @param imgPath path to the image file
     */
    public ImagePanel(String imgPath) {
        this.img = new MapImage(imgPath).getImage();

        if (img != null) {
            Dimension size = new Dimension(this.img.getWidth(null), this.img.getHeight(null));
            super.setPreferredSize(size);
        } else {
            System.out.println("AT THAT MOMENT, JVM KNEW...DEVELOPER FUCKED UP -- check your image path!");
        }
    }

    /**
     * Drawing map image on the JPanel
     *
     * @param g Graphics object(from: {@link javax.swing.JComponent} (remember: {@link javax.swing.JPanel} extends {@link javax.swing.JComponent})) we are going to draw onto
     */
    public void paintComponent(Graphics g) {
        g.drawImage(this.img, 0, 0, null);
    }


}
