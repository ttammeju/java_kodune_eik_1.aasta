package kodune;

import java.awt.*;

/**
 * Gets mouse absolute position and color on specific coordinates
 *
 * @author Tarvo Tammejuur
 * @version 1.0
 */
public class Mouse {
    private static Robot mouseRobot;

    /**
     * Initialises the <i>STATIC</i> {@link #mouseRobot} - no constructor (therefore object creation) needed
     * @see Robot
     */
    static {
        try {
            mouseRobot = new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        }
    }

    /**
     * Moves the mouse to the specified coordinate
     *
     * @param x absolute x-coordinate
     * @param y absolute y-coordinate
     */
    public static void move(int x, int y) {
        mouseRobot.mouseMove(x, y);
    }

    /**
     * gets the mouse absolute position on the screen
     *
     * @return mouse absolute position
     * @see PointerInfo#getLocation()
     */
    public static Point getPosition() {
        Point mousePoint = MouseInfo.getPointerInfo().getLocation();
        return mousePoint;
    }

    /**
     * Returns the Color ({@link Color}) on x- and y-coordinates
     *
     * @param p point representing the x- and y-coordinates
     * @return color under the point
     * @see Robot#getPixelColor(int, int)
     */
    public static Color getPointColor(Point p) {
        Color mouseColor = mouseRobot.getPixelColor(p.x, p.y);
        return mouseColor;
    }

    /**
     * Gets the color "under" the absolute position of the mouse on screen
     *
     * @return Color ({@link Color}) under the mouse
     * @see Robot#getPixelColor(int, int)
     */
    public static Color getMouseColor() {
        int x = getPosition().x;
        int y = getPosition().y;

        Color mouseColor = mouseRobot.getPixelColor(x, y);
        return mouseColor;
    }

}