package kodune;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;

/**
 * Keeping track of scores, saving them to file
 *
 * @author Tarvo Tammejuur
 * @version 1.0
 */
public class Score {
    private static String playerName;
    private static ArrayList<Long> scoreList = new ArrayList<>();

    /**
     * Initialising the player name
     *
     * @param pName player name
     */
    public Score(String pName) {
        playerName = pName;
    }

    /**
     * adding a score to current session' score-list
     *
     * @param millis total time in milliseconds ({@link Game#calculateTotalTime()})
     * @see #scoreList
     */
    public static void add(Long millis) {
        scoreList.add(millis);
    }

    /**
     * Saving the best session score to file
     *
     * @see #getSessBest()
     */
    public static void writeToFile() {
        File f = new File("scores.csv");
        if (f.exists() && f.canWrite()) {
            try {
                PrintStream ps = new PrintStream(new FileOutputStream(f, true));
                Long sessBest = getSessBest();
                if (sessBest != -1L) {
                    ps.append(playerName + ":" + sessBest + ", ");
                    ps.println();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Getting the best score(total time, in milliseconds) from current session scores ({@link #scoreList})
     *
     * @return best session score or -1L if score-list is empty
     * @see #scoreList
     */
    private static Long getSessBest() {
        if (scoreList.size() > 0) {
            Long best = scoreList.get(0);
            for (Long score : scoreList) {
                if (best > score) {
                    best = score;
                }
            }
            return best;
        }
        return -1L;
    }

    /**
     * Printing current session scores
     */
    public void printScores() {
        System.out.println("Printing scores for: " + playerName);
        for (Long s : scoreList) {
            System.out.println(s);
        }
        System.out.println("-   -   -   -");
    }
}
