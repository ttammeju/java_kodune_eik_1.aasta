package kodune;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.concurrent.TimeUnit;

/**
 * 3 main colors used throughout the game.
 * As enums for easy human-readable code and because of type-safety
 */
enum MAP_COLOR {
    START(237, 28, 36), // red-ish
    END(255, 242, 0), // yellow-ish
    PATH(0, 0, 0); // black

    Color color;

    MAP_COLOR(int r, int g, int b) {
        color = new Color(r, g, b);
    }
}

/**
 * Main program logic in this class
 *
 * @author Tarvo Tammejuur
 * @version 1.0
 */

public class Game {
    private boolean started = false; // game state
    private Screen screen; // actual JFrame window
    private long startTime, finishTime;
    private int level = 1; // game starts at level 1
    private MouseListenerThread mouseMonitor = null; // monitors the mouse position
    private Score score;

    /**
     * Initialises the window and loads(displays) a map (image)
     *
     * @param screen references the main window
     */
    public Game(Screen screen) {
        this.screen = screen;
        screen.loadMap("levels/level" + level + ".png");
    }

    /**
     * in which state (true - running / false - stopped) the game is in?
     *
     * @return return the game state
     * @see #screen
     */
    public boolean isStarted() {
        return started;
    }

    /**
     * changes the game running/ started state
     *
     * @param gameStarted new state (true - running / false - stopped)
     */
    public void setStarted(boolean gameStarted) {
        this.started = gameStarted;
    }

    /**
     * Current screen window, the map image is load onto
     *
     * @return The main (and only) window
     */
    public Screen getScreen() {
        return screen;
    }

    /**
     * Time between starting the game and finishing the game
     *
     * @return Total time between start and finish
     */
    public long calculateTotalTime() {
        printFormatMillis(finishTime - startTime);
        return finishTime - startTime;
    }

    /**
     * Current level user is on at the moment
     *
     * @return Current level number
     */
    public int getLevel() {
        return level;
    }

    /**
     * Used to change the level
     *
     * @param level new level number
     */
    public void setLevel(int level) {
        this.level = level;
    }

    /**
     * Saving current time in milliseconds (for future calculations)
     *
     * @see #calculateTotalTime()
     */
    public void startTimer() {
        this.startTime = System.currentTimeMillis();
    }

    /**
     * Saving current time in milliseconds (for future calculations)
     *
     * @see #calculateTotalTime()
     */
    public void stopTimer() {
        this.finishTime = System.currentTimeMillis();
    }

    /**
     * Converts milliseconds to seconds
     * NOTE: Do not use it in precision-sensitive calculations
     *
     * @param millis milliseconds to be converted
     * @return nice String representation of milliseconds
     */
    private String printFormatMillis(long millis) {
        return String.format("%d sec",
                TimeUnit.MILLISECONDS.toSeconds(millis));
    }

    /**
     * Starting the game...
     * Asking the user name for the Score class ({@link Score#playerName})<br>
     * Initialising mouse listeners ({@link #initMouseListeners(Game)})<br>
     * Initialising mouse monitor ({@link MouseListenerThread})<br>
     */
    public void start() {
        // asks user for a name
        this.getUserName();

        // init mouse listeners
        this.initMouseListeners(this);

        // thread activity
        this.mouseMonitor = new MouseListenerThread(this);
        this.mouseMonitor.start();
        System.out.println(this.isStarted());
    }

    /**
     * Getting the user name and initialising static {@link Score} class with user name
     */
    private void getUserName() {
        String choice;
        do {
            choice = JOptionPane.showInputDialog(null, "Enter your name:", "your name", JOptionPane.PLAIN_MESSAGE);
        } while (choice.equals(""));
        score = new Score(choice);
    }

    /**
     * Initialises mouse listeners for detecting the movement and mouse-release actions
     * Main "action"-calls here
     *
     * @param currentGame current game object for invoking its methods
     */
    private void initMouseListeners(Game currentGame) {
        this.getScreen().getPanel().addMouseListener(new MouseAdapter() {
            public void mouseReleased(MouseEvent e) {
                if (Mouse.getMouseColor().equals(MAP_COLOR.START.color)) {
                    System.out.println("- - - GAME STARTED - - -");
                    currentGame.setStarted(true);
                    currentGame.startTimer();
                }
            }
        });
        this.getScreen().getPanel().addMouseMotionListener(new MouseAdapter() {
            public void mouseMoved(MouseEvent e) {

                if (started) {
                    Color mColor = Mouse.getMouseColor();
                    if (mColor.equals(MAP_COLOR.START.color) || mColor.equals(MAP_COLOR.PATH.color)) {
                        System.out.println("@ track");
                    } else if (mColor.equals(MAP_COLOR.END.color)) { // mouse on (yellow) finish color
                        currentGame.nextLevel();
                    } else {
                        currentGame.endGame();
                    }
                }
            }
        });
    }

    /**
     * Displaying the result score and resetting the game to level 1
     */
    public void endGame() { // resetting everything; displaying result
        JOptionPane.showMessageDialog(
                null,
                "Start All Over Again",
                "restart",
                JOptionPane.INFORMATION_MESSAGE
        );

        this.stopTimer();
        this.setStarted(false);
        getScreen().setTitle("time: " + this.calculateTotalTime() + "ms level: " + this.getLevel());
        this.setLevel(0);
        this.nextLevel();
    }

    /**
     * Incrementing the level.
     * Checks whether it's the last level, otherwise loading the next map(level image) {@link Screen#loadMap(String)}
     * if it's the last level: resetting the game and saving the user' score {@link Score#add(Long)}.
     * NOTE: not saving scores to file! {@link Score#writeToFile()} does this in {@link Screen#Screen()} in its constructor
     */
    public void nextLevel() {
        if (this.getLevel() == 2) { //TODO: change to constant or count "level" + X + ".png" files!
            this.endGame();
            Score.add(this.calculateTotalTime());
            score.printScores();
            System.out.println("No more levels. Total time: " + this.printFormatMillis(this.calculateTotalTime()));
            return;
        }
        this.level++;
        this.getScreen().loadMap("levels/level" + level + ".png");
    }
}
